#!/bin/bash

# Environment for subversion scripts 
export SVNSERVE_HOST=localhost
export SVNSERVE_PORT=3690
export SVNSERVE_BASE_DIR=~/svnserve
export SVNSERVE_BASE_URL=svn://${SVNSERVE_HOST}

# log function
log () {
    mode=$1
    shift
    if [ "x${mode}" = "xdebug" ] ; then
        if [ "${VERBOSE:-false}" != "true" ] ; then
            return
        fi
        prefix="DEBUG  "
    elif [ "x${mode}" = "xinfo" ] ; then
        prefix="INFO   "
    elif [ "x${mode}" = "xerror" ] ; then
        prefix="ERROR  "
    fi
    echo "${prefix} | $@" >&2
}