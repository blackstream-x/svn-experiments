#!/bin/bash
set -euo pipefail

# SVN server control

SCRIPT_DIR=$(dirname $0)
source ${SCRIPT_DIR}/svn-environment.sh

get_pid () {
    # Print svnserve process ID to stdout
    pid_from_netstat=$(ss -tlpn | awk '$4 == "127.0.0.1:'${SVNSERVE_PORT}'" { gsub(/^.+,pid=/, ""); gsub(/,.*$/, ""); print}')
    pid_from_process=$(ps -ef | awk '$8 == "svnserve" { print $2 }')
    if [ "x${pid_from_netstat}" = "x${pid_from_process}" ] ; then
        log debug "Process ID read from netstat"
        echo "${pid_from_netstat}"
    else
        log debug "Process ID read from proc"
        echo "${pid_from_process}"
    fi
}

is_running () {
    pid=$(get_pid)
    [ ! -z "${pid}" ]
}

exit_usage () {
    log error "Usage: $0 {start|stop|status}"
    exit 1
}

if [ $# -lt 1 ] ; then
    exit_usage
fi

case $1 in
    start)
        if is_running ; then
            log error "svnserve already running, no start command issued"
        else
            log info "starting svnserve (svn://${SVNSERVE_HOST}:${SVNSERVE_PORT}) ..."
            svnserve --listen-host ${SVNSERVE_HOST} --listen-port ${SVNSERVE_PORT} -d -r ${SVNSERVE_BASE_DIR}
            log info "... done"
        fi
        ;;
    stop)
        if is_running ; then
            log info "stopping svnserve ..."
            kill -1 $(get_pid)
            log info "... done"
        else
            log error "svnserve not running, no stop command issued"
        fi
        ;;
    status)
        if is_running ; then
            log info "svnserve runs with process id $(get_pid)"
        else
            log info "svnserve is not running"
        fi
        ;;
    *)
        exit_usage
        ;;
esac
