#!/bin/bash
set -euo pipefail

# Create SVN repositories

SCRIPT_DIR=$(dirname $0)
source ${SCRIPT_DIR}/svn-environment.sh

if [ $# -lt 1 -o "x$1" = "x-h" ] ; then
    log error "Usage: $0 REPONAME [ USER PASSWORD ]"
    exit 1
fi

repo_name=$1
repo_full_path=${SVNSERVE_BASE_DIR}/${repo_name}
log info "Creating repository at ${repo_full_path} ..."
svnadmin create ${repo_full_path}
log info "... done"

if [ $# -gt 2 ] ; then
    repo_username=$2
    repo_password=$3
    log info "Adding write access user ${repo_username} with password ${repo_password} ..."
    sed "s/REPOSITORYNAME/${repo_name}/" ${SCRIPT_DIR}/templates/conf/svnserve.conf > ${SVNSERVE_BASE_DIR}/${repo_name}/conf/svnserve.conf
    sed "s/WRITEUSER/${repo_username}/" ${SCRIPT_DIR}/templates/conf/passwd | sed "s/WRITEPASSWORD/${repo_password}/" > ${SVNSERVE_BASE_DIR}/${repo_name}/conf/passwd
    log info "... done"
fi

svn info ${SVNSERVE_BASE_URL}/${repo_name}
